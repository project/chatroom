<?php


namespace Drupal\chatroom;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the chatroom entity type.
 */
class ChatroomViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    return $data;
  }

}
