<?php


namespace Drupal\chatroom;

use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the chatroom schema handler.
 */
class ChatroomStorageSchema extends SqlContentEntityStorageSchema {

}

